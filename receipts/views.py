from django.db.models import Count
from django.http import HttpRequest, HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from receipts.models import ExpenseCategory, Account, Receipt


@login_required
def receipt_list(request: HttpRequest) -> HttpResponse:
    """
    Create view that will get all of instances of Receipt model
    and put them in context for template.

    Feature 8 requires someone to log in before accessing it.
    It also constrains the receipts shown to only those that the
    logged-in person purchased.
    """
    # receipts = Receipt.objects.all() we modified this in Feature 8
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/home.html", context)


@login_required
def category_list(request):
    categories = (
        ExpenseCategory.objects.filter(receipts__purchaser=request.user)
        .annotate(receipt_count=Count("receipts"))
        .distinct()
    )
    context = {
        "category_list": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    accounts = (
        Account.objects.filter(receipts__purchaser=request.user)
        .annotate(receipt_count=Count("receipts"))
        .distinct()
    )
    context = {
        "account_list": accounts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/accounts/create_account.html", context)
