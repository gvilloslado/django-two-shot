from django.db import models
from django.conf import settings
from django.utils import timezone


class ExpenseCategory(models.Model):
    """
    Model is a value to apply to receipts like 'gas' or 'entertainment.'
    """

    # name property that contains characters with a max length of 50
    name = models.CharField(max_length=50)

    # owner property (foreign key) to User model
    owner = models.ForeignKey(  # many categories to one owner
        settings.AUTH_USER_MODEL,  # referring to User model
        related_name="categories",  # related name "categories"
        on_delete=models.CASCADE,  # cascade deletion relation
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Account(models.Model):
    """
    This is not for user accounts, but for accounts like a bank account.
    The way that we paid for it.
    """

    # name property that contains characters with max length 100
    name = models.CharField(max_length=100)

    # number property that contains characters (not numbers) with max length 20
    number = models.CharField(max_length=20)

    # owner property (foreign key) to User model
    owner = models.ForeignKey(  # many accounts to one owner
        settings.AUTH_USER_MODEL,  # referring to User model
        related_name="accounts",  # related name "accounts"
        on_delete=models.CASCADE,  # cascade deletion relation
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Receipt(models.Model):
    """
    This model is primary thing that this apps keeps track of.
    """

    # vendor property that contains characters with max length 200
    vendor = models.CharField(max_length=200)

    # total property (DecimalField) with 3 decimal places max 10 digits
    total = models.DecimalField(max_digits=10, decimal_places=3)

    # tax property (DecimalField) with 3 decimal places max 10 digits
    tax = models.DecimalField(max_digits=10, decimal_places=3)

    # date property that contains date and time of when transaction took place
    date = models.DateTimeField(
        default=timezone.now
    )  # user can override this default by explicitly setting the date field

    # purchaser property foreign key to User model
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",  # related name 'receipts'
        on_delete=models.CASCADE,  # cascade deletion relation
    )

    # category property foreign key to ExpenseCategory model
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",  # related name 'receipts'
        on_delete=models.CASCADE,  # cascade deletion relation
    )

    # account property foreign key to Account model
    account = models.ForeignKey(
        Account,
        related_name="receipts",  # related name 'receipts'
        on_delete=models.CASCADE,  # cascade deletion relation
        null=True,  # allowed to be null
    )

    class Meta:
        ordering = ["-date"]
